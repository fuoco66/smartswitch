// load the config from memory
DynamicJsonDocument WebGetAccounts(){

	DynamicJsonDocument jsonDoc(1024);
	JsonObject root = jsonDoc.to<JsonObject>();
	
	// root["maxBtnCount"] = BtnData.maxBtnCount;
	root["accountsCount"] = AccountsData.accountsCount;
	
	JsonArray accounts = root.createNestedArray("accounts");

	// iterates for every button
	for (int i = 0; i < AccountsData.accountsCount; i++){

		JsonObject account = accounts.createNestedObject();
		
		account["id"] = AccountsData.Accounts[i].id;
		account["email"] = AccountsData.Accounts[i].email;
		account["key"] = AccountsData.Accounts[i].key;

	}

	return jsonDoc;

}



bool CreateAccountsFile(){
	DynamicJsonDocument jsonDoc(1024);
	jsonDoc["accountsCount"] = 0;

	// compute the required size
	const size_t CAPACITY = JSON_ARRAY_SIZE(0);

	// allocate the memory for the document
	StaticJsonDocument<CAPACITY> doc;

	// create an empty array
	JsonArray array = doc.to<JsonArray>();
	jsonDoc["accounts"] = doc;

	File AccountsFile = SPIFFS.open(accountsFile, "w");
	if (!AccountsFile) {
		Sprintln("Failed to create accounts file");
		return false;
	}

	// serializeJson(jsonDoc, Serial);
	serializeJson(jsonDoc, AccountsFile);

	AccountsFile.close();
	Sprintln("File created");

	return true;
}

// Load the buttons config from file
void LoadAccounts(){

	if (!SPIFFS.begin()) {
		Sprintln("Accounts SPIFF FAILED");
		return;
	}
	
	Sprintln("Accounts Spiff begin");

	if(!SPIFFS.exists(accountsFile)) {
		Sprintln("File NOT found, creating");
		if(CreateAccountsFile() != true){
			return;
		}
	}
		
	Sprintln("Accounts File found");
	
	//file exists, reading and loading
	File objAccountsFile = SPIFFS.open(accountsFile, "r");

	if(!objAccountsFile){
		Sprintln("Failed to open Accounts file");
		return;
	}

	// Sprintln("Button File opened");
	// for debug, print the whole file
	// while (objAccountsFile.available()) {

	//     Serial.write(objAccountsFile.read());
	// }

	size_t size = objAccountsFile.size();
	// Allocate a buffer to store contents of the file.
	std::unique_ptr<char[]> buf(new char[size]);

	objAccountsFile.readBytes(buf.get(), size);

	DynamicJsonDocument jsonBuffer(1024);
	DeserializationError jsonError = deserializeJson(jsonBuffer, buf.get());
	
	if (jsonError){
		Sprintln("Failed to parse Button json: ");
		switch (jsonError.code()) {
			case DeserializationError::InvalidInput:
					Sprintln(F("Invalid input!"));
					break;
			case DeserializationError::NoMemory:
					Sprintln(F("Not enough memory"));
					break;
			default:
					Sprintln(F("Deserialization failed"));
					break;
		}

		objAccountsFile.close();
		return;
	}

	Sprintln("Button File Loaded");

	// load buttons config in memory
	
	AccountsData.accountsCount = jsonBuffer["accountsCount"];
	
	for (int i = 0; i < AccountsData.accountsCount; i++){
		AccountsData.Accounts[i].id = jsonBuffer["accounts"][i]["id"].as<int>();
		
		strcpy(AccountsData.Accounts[i].email, jsonBuffer["accounts"][i]["email"].as<char*>());
		strcpy(AccountsData.Accounts[i].key, jsonBuffer["accounts"][i]["key"].as<char*>());

	}

}

