// #include "Hardware.h"

void ReadSettings(){

	if (SPIFFS.begin()) {
		Sprintln("Settings Spiff begin");

		if (SPIFFS.exists(settingsfile)) {
			
			Sprintln("Settings File found");
			
			//file exists, reading and loading
			File configFile = SPIFFS.open(settingsfile, "r");

			if (configFile) {
				Sprintln("Settings File opened");

				size_t size = configFile.size();
				// Allocate a buffer to store contents of the file.
				std::unique_ptr<char[]> buf(new char[size]);

				configFile.readBytes(buf.get(), size);
				// DynamicJsonBuffer jsonBuffer;
				DynamicJsonDocument jsonBuffer(size);
				// JsonObject json = jsonBuffer.parseObject(buf.get());
				
				DeserializationError jsonError = deserializeJson(jsonBuffer, buf.get());
			
				if (jsonError){
					Sprintln("failed to load json Settings");
					configFile.close();
  				return;
				}

				Sprintln("");
				// print JSON to serial
				// serializeJson(jsonBuffer, Serial);
				Sprintln("");

				Sprintln("Load settings");

				strcpy(SettingsData.host, jsonBuffer["host"]);
				Sprintln("Loadaded host");

				strcpy(SettingsData.ifttt_key, jsonBuffer["ifttt_key"]);
				Sprintln("Loadaded ifttt_key");

				strcpy(SettingsData.update_username, jsonBuffer["update_username"]);
				Sprintln("Loadaded update_username");

				strcpy(SettingsData.update_password, jsonBuffer["update_password"]);
				Sprintln("Loadaded update_password");

				strcpy(SettingsData.update_path, jsonBuffer["update_path"]);
				Sprintln("Loadaded update_path");

				// close file
				configFile.close();

			}
		}
		else{
			Sprintln("File NOT found");
		}
	} 
	else {
		Sprintln("SPIFF FAILED");
	}
}

void SaveSettings(){

	DynamicJsonDocument jsonDoc(1024);
	jsonDoc["host"] = SettingsData.host;
	jsonDoc["ifttt_key"] = SettingsData.ifttt_key;

	jsonDoc["update_username"] = SettingsData.update_username;
	jsonDoc["update_password"] = SettingsData.update_password;
	jsonDoc["update_path"] = SettingsData.update_path;

	File configFile = SPIFFS.open(settingsfile, "w");
	if (!configFile) {
	}

	// serializeJson(jsonDoc, Serial);
	serializeJson(jsonDoc, configFile);

	configFile.close();
}

void WifiReset(){
	ObjGlobal_WifiManager.resetSettings();
	ESP.reset();
}

void FullReset(){

	if (SPIFFS.begin()) {
		// TODO implement better way
		if (SPIFFS.exists(settingsfile)) {
			SPIFFS.remove(settingsfile);
		}
		if (SPIFFS.exists(configfile)) {
			SPIFFS.remove(configfile);
		}
		if (SPIFFS.exists(buttonsFile)) {
			SPIFFS.remove(buttonsFile);
		}
		if (SPIFFS.exists(accountsFile)) {
			SPIFFS.remove(accountsFile);
		}

		SPIFFS.end();

	}
  WifiReset();
}

void Reboot(){
	ESP.reset();
}

bool CreateButtonsConfigFile(){
	DynamicJsonDocument jsonDoc(1024);
	jsonDoc["maxBtnCount"] = 0;
	jsonDoc["configBtnCount"] = 0;

	// compute the required size
	const size_t CAPACITY = JSON_ARRAY_SIZE(0);

	// allocate the memory for the document
	StaticJsonDocument<CAPACITY> doc;

	// create an empty array
	JsonArray array = doc.to<JsonArray>();
	jsonDoc["buttons"] = doc;

	File ButtonsConfigFile = SPIFFS.open(buttonsFile, "w");
	if (!ButtonsConfigFile) {
		Sprintln("Failed to create file");
		return false;
	}

	// serializeJson(jsonDoc, Serial);
	serializeJson(jsonDoc, ButtonsConfigFile);

	ButtonsConfigFile.close();
	Sprintln("File created");

	return true;
}

// Load the buttons config from file
void LoadButtonsConfig(){

	if (!SPIFFS.begin()) {
		Sprintln("Button SPIFF FAILED");
		return;
	}
	
	Sprintln("Button Spiff begin");

	if(!SPIFFS.exists(buttonsFile)) {
		Sprintln("File NOT found, creating");
		if(CreateButtonsConfigFile() != true){
			return;
		}
	}
		
	Sprintln("Button File found");
	
	//file exists, reading and loading
	File objButtonsFile = SPIFFS.open(buttonsFile, "r");

	if(!objButtonsFile){
		Sprintln("Failed to open Button file");
		return;
	}

	// Sprintln("Button File opened");
	// for debug, print the whole file
	// while (objButtonsFile.available()) {

	//     Serial.write(objButtonsFile.read());
	// }

	size_t size = objButtonsFile.size();
	// Allocate a buffer to store contents of the file.
	std::unique_ptr<char[]> buf(new char[size]);

	objButtonsFile.readBytes(buf.get(), size);

	DynamicJsonDocument jsonBuffer(1024);
	DeserializationError jsonError = deserializeJson(jsonBuffer, buf.get());
	
	if (jsonError){
		Sprintln("Failed to parse Button json: ");
		switch (jsonError.code()) {
			case DeserializationError::InvalidInput:
					Sprintln(F("Invalid input!"));
					break;
			case DeserializationError::NoMemory:
					Sprintln(F("Not enough memory"));
					break;
			default:
					Sprintln(F("Deserialization failed"));
					break;
		}

		objButtonsFile.close();
		return;
	}

	Sprintln("Button File Loaded");

	// load buttons config in memory
	
	BtnData.maxBtnCount = jsonBuffer["maxBtnCount"];
	BtnData.configBtnCount = jsonBuffer["configBtnCount"];
	
	for (int i = 0; i < BtnData.configBtnCount; i++){
		BtnData.Buttons[i].pin = jsonBuffer["buttons"][i]["pin"].as<int>();
		BtnData.Buttons[i].active = jsonBuffer["buttons"][i]["active"].as<bool>(); 
		BtnData.Buttons[i].mqtt = jsonBuffer["buttons"][i]["mqtt"].as<bool>(); 
		BtnData.Buttons[i].custom = jsonBuffer["buttons"][i]["custom"].as<bool>(); 

		strcpy(BtnData.Buttons[i].name, jsonBuffer["buttons"][i]["name"].as<char*>());
		strcpy(BtnData.Buttons[i].pressArg, jsonBuffer["buttons"][i]["pressArg"].as<char*>());
		strcpy(BtnData.Buttons[i].longPressArg, jsonBuffer["buttons"][i]["longPressArg"].as<char*>());

		BtnData.Buttons[i].state = LOW; 
		BtnData.Buttons[i].secondState = LOW; 
		BtnData.Buttons[i].longPress = LOW; 
		BtnData.Buttons[i].lastDebounceTime = 0;
		BtnData.Buttons[i].longPressTime = 0;

	}

}

void SaveConfigFile(String payLoad, String strFileName){

	DynamicJsonDocument jsonBuffer(1024);
	DeserializationError jsonError = deserializeJson(jsonBuffer, payLoad);

	if (jsonError){
		Sprintln("Failed to parse json");
		return;
	}

	File objFile = SPIFFS.open(strFileName, "w");
	if (!objFile) {
		Sprintln("Failed to open file");
		return;
	}

	Sprintln("Writing file");
	// serializeJson(jsonBuffer["value"], Serial);

	serializeJson(jsonBuffer["value"], objFile);
	Sprintln("File Written");

	objFile.close();
		
	Sprintln("Writing END");
	
}

// load the config from memory
DynamicJsonDocument WebGetButtonsConfig(){

	DynamicJsonDocument jsonDoc(1024);
	JsonObject root = jsonDoc.to<JsonObject>();
	
	root["maxBtnCount"] = BtnData.maxBtnCount;
	root["configBtnCount"] = BtnData.configBtnCount;
	
	JsonArray buttons = root.createNestedArray("buttons");

	// iterates for every button
	for (int i = 0; i < BtnData.configBtnCount; i++){

		JsonObject btn = buttons.createNestedObject();
		
		btn["pin"] = BtnData.Buttons[i].pin;
		btn["active"] = BtnData.Buttons[i].active;
		btn["mqtt"] = BtnData.Buttons[i].mqtt;
		btn["custom"] = BtnData.Buttons[i].custom;

		btn["name"] = BtnData.Buttons[i].name;
		btn["pressArg"] = BtnData.Buttons[i].pressArg;
		btn["longPressArg"] = BtnData.Buttons[i].longPressArg;
	
	}

	return jsonDoc;

}

bool CreateConfigFile(){
	
	DynamicJsonDocument jsonDoc(1024);

	// load default data from struct init	

	JsonArray ledLevels = jsonDoc.createNestedArray("ledLevels");
	for (byte i = 0; i < (sizeof(ConfigurationData.ledLevels) / sizeof(ConfigurationData.ledLevels[0])); i++) {
		ledLevels.add(ConfigurationData.ledLevels[i]);
	}

	jsonDoc["autoTimeDimm"] = ConfigurationData.autoTimeDimm;
	jsonDoc["secondaryStatusLed"] = ConfigurationData.secondaryStatusLed;
	jsonDoc["dayHour"] = ConfigurationData.dayHour;
	jsonDoc["dayMinutes"] = ConfigurationData.dayMinutes;
	jsonDoc["nightHour"] = ConfigurationData.nightHour;
	jsonDoc["nightMinutes"] = ConfigurationData.nightMinutes;

	File ConfigFile = SPIFFS.open(configfile, "w");
	if (!ConfigFile) {
		Sprintln("Failed to create file");
		return false;
	}

	serializeJson(jsonDoc, Serial);
	serializeJson(jsonDoc, ConfigFile);

	ConfigFile.close();
	Sprintln("File created");

	return true;
}

// Load the buttons config from file
void LoadConfig(){

	if (!SPIFFS.begin()) {
		Sprintln("Config SPIFF FAILED");
		return;
	}
	
	Sprintln("Config Spiff begin");

	if(!SPIFFS.exists(configfile)) {
		Sprintln("File NOT found, creating");
		if(CreateConfigFile() != true){
			return;
		}
	}
		
	Sprintln("Config File found");
	
	//file exists, reading and loading
	File objConfigFile = SPIFFS.open(configfile, "r");

	if(!objConfigFile){
		Sprintln("Failed to open Button file");
		return;
	}

	// Sprintln("Button File opened");
	// for debug, print the whole file
	// while (objConfigFile.available()) {

	//     Serial.write(objConfigFile.read());
	// }

	size_t size = objConfigFile.size();
	// Allocate a buffer to store contents of the file.
	std::unique_ptr<char[]> buf(new char[size]);

	objConfigFile.readBytes(buf.get(), size);

	DynamicJsonDocument jsonBuffer(1024);
	DeserializationError jsonError = deserializeJson(jsonBuffer, buf.get());
	
	if (jsonError){
		Sprintln("Failed to parse config json: ");
		switch (jsonError.code()) {
			case DeserializationError::InvalidInput:
					Sprintln(F("Invalid input!"));
					break;
			case DeserializationError::NoMemory:
					Sprintln(F("Not enough memory"));
					break;
			default:
					Sprintln(F("Deserialization failed"));
					break;
		}

		objConfigFile.close();
		return;
	}

	Sprintln("config File Loaded");

	for (byte i = 0; i < (sizeof(ConfigurationData.ledLevels) / sizeof(ConfigurationData.ledLevels[0])); i++) {
		ConfigurationData.ledLevels[i] = jsonBuffer["ledLevels"][i].as<int>();
	}

	ConfigurationData.autoTimeDimm = jsonBuffer["autoTimeDimm"].as<bool>();
	ConfigurationData.secondaryStatusLed = jsonBuffer["secondaryStatusLed"].as<bool>();
	ConfigurationData.dayHour = jsonBuffer["dayHour"];
	ConfigurationData.dayMinutes = jsonBuffer["dayMinutes"];
	ConfigurationData.nightHour = jsonBuffer["nightHour"];
	ConfigurationData.nightMinutes = jsonBuffer["nightMinutes"];
	
}

// load the config from memory
DynamicJsonDocument WebGetConfig(){

	DynamicJsonDocument jsonDoc(1024);
	JsonObject root = jsonDoc.to<JsonObject>();

	JsonArray ledLevels = jsonDoc.createNestedArray("ledLevels");
	for (byte i = 0; i < (sizeof(ConfigurationData.ledLevels) / sizeof(ConfigurationData.ledLevels[0])); i++) {
		ledLevels.add(ConfigurationData.ledLevels[i]);
	}

	root["autoTimeDimm"] = ConfigurationData.autoTimeDimm;
	root["secondaryStatusLed"] = ConfigurationData.secondaryStatusLed;
	root["dayHour"] = ConfigurationData.dayHour;
	root["dayMinutes"] = ConfigurationData.dayMinutes;
	root["nightHour"] = ConfigurationData.nightHour;
	root["nightMinutes"] = ConfigurationData.nightMinutes;
	
	return jsonDoc;

}