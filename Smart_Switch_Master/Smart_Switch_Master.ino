// TODO
// Correct btn settings, if the btns are disabled they should not be cancelled
// add custom for long and short press
// rework initial config
// rework file clean
// implement safer way to load settings(is json key not found reboot loop)

#include "Hardware.h"

//Form Custom SSID
String ssidAP = "SmartBtn_1_" + String(ESP.getChipId());

//flag for saving data
bool shouldSaveConfig = false;

const char* htmlfile = "/index.html";
const char* settingsfile = "/settings.json";
const char* buttonsFile = "/buttons.json";
const char* configfile = "/config.json";
const char* accountsFile = "/accounts.json";

ESP8266WebServer ObjGlobal_HttpServer(80);
ESP8266HTTPUpdateServer httpUpdater;

WiFiManager ObjGlobal_WifiManager;

Ticker StatusLedTicker;

HwBtn InputBtn[COUNT_HWBTN];
TimedAction objBtnThread = TimedAction(100, CheckBtn);

Timezone TimeZone;

WiFiClient WifiNetwork;
PubSubClient MqttClient(WifiNetwork);

// Status led Ticker
void tick() {
  //toggle state
  int MainLedState = digitalRead(STATUS_LED_1);  // get the current state of GPIO1 pin
  digitalWrite(STATUS_LED_1, !MainLedState);     // set pin to the opposite state

  // manual on/off because settings are not loaded yet
  int SecondaryLedState = digitalRead(STATUS_LED_2);  // get the current state of GPIO1 pin
  digitalWrite(STATUS_LED_2, !SecondaryLedState);     // set pin to the opposite state

}

void ToggleSecondLed(){
  // get the current state of GPIO2 pin and inverts it
  bool SecondaryLedState = digitalRead(STATUS_LED_2) == HIGH ? false : true;  
  
  ToggleSecondLed(SecondaryLedState);
}

void ToggleSecondLed(bool status){

  if(!ConfigurationData.secondaryStatusLed){
    digitalWrite(STATUS_LED_2, HIGH);
    return;
  }

  // set pin, GPIO2 is inverted
  digitalWrite(STATUS_LED_2, status ? LOW : HIGH);

}

// load default value at startup
int globalStatusLedLevel = 0;//ConfigurationData.ledLevel_2;

void FixedLed(int ledLevel = NULL) {

  if(ledLevel == NULL){
    ledLevel = ConfigurationData.ledLevels[2];
  }

  StatusLedTicker.detach();
  // digitalWrite(STATUS_LED_1, HIGH);
  globalStatusLedLevel = ledLevel;
  analogWrite(STATUS_LED_1, ledLevel);

  // Secondary Led ON
  ToggleSecondLed(true);
}

void setup() {

  // Serial.begin(115200);
  Sbegin(115200);

  // Setup status led
  pinMode(STATUS_LED_1, OUTPUT);
  digitalWrite(STATUS_LED_1, HIGH);

  // // Secondary Led ON
  pinMode(STATUS_LED_2, OUTPUT);
  digitalWrite(STATUS_LED_2, LOW);

  // start ticker with 0.6 because we start in AP mode and try to connect
  StatusLedTicker.attach(0.2, tick);

  ReadSettings();

  SetupConfigMode(ObjGlobal_WifiManager);
  
  LoadConfig();
  
  LoadAccounts();

  LoadButtonsConfig();
  
  SetupBtn();

  // Setup web updater
  httpUpdater.setup(&ObjGlobal_HttpServer, SettingsData.update_path, SettingsData.update_username, SettingsData.update_password);

  SetupWebHandlers();

  SetupTime();

  MqttClient.setServer("192.168.1.2",1883);
  
  while (!MqttClient.connect("ESP", "pi", "f4e7889@@@")){
    Sprintln("MQTT CONNECTING");
    delay(1000);
  }

  Sprintln(MqttClient.state());
}

void loop() {
  // delay(10); // convert with millis

  events();

  MqttClient.loop();

  ObjGlobal_HttpServer.handleClient();

  objBtnThread.check();
  int _button = ReadBtn();

  if(WiFi.status() == WL_CONNECTED && //Check WiFi connection status
      _button != 99) { 
    //Sprintln("Connected");
    //Sprintln(_button);

    for(int i = 0; i < COUNT_HWBTN; i++){

      // look for the pressed button
      if(_button == BtnData.Buttons[i].pin ||
         _button == LongP(BtnData.Buttons[i].pin)){
        
        // button found, take action
        ManagePressedButton(_button, i);
        break;
      }

      // error condition
      if(i == COUNT_HWBTN-1){
        Sprintln("BUTTON NOT ACTIVE");
      }
      
    }
    
  }

}

void Blink(uint8_t times, uint8_t waitTime){

  for (uint8_t i = 0; i < times; i++){
    
    // digitalWrite(STATUS_LED_2, HIGH);   
    ToggleSecondLed(false);

    digitalWrite(STATUS_LED_1, LOW);   
    delay(waitTime);                       
    FixedLed(globalStatusLedLevel);
    ToggleSecondLed(true);
    // digitalWrite(STATUS_LED_2, LOW);   
    
    if(times != 1 && i != times -1){
      delay(waitTime/2);                       
    }
  }
}

void ManagePressedButton(int btnPin, int btnIndex){

  // if custom, execute custom rutine
  if(BtnData.Buttons[btnIndex].custom){
    Sprint("It's custom");
    return;
  }

  // continue with ifft rutine
  String strPressArg = String(BtnData.Buttons[btnIndex].pressArg);
  String strLongPressArg = String(BtnData.Buttons[btnIndex].longPressArg);
  String strCurrentArg = "";
  int blinkCount = 3;

  if(strPressArg.length() == 0 && strLongPressArg.length() == 0){
    Sprintln("No argument configured");
    Blink(3, 200);
    return;
  }
  
  if(btnPin > 0){
    Sprint("Short press, pin: ");
    Sprint(btnPin);
    Sprint(" , ");

    // if no argument for press, use longpress
    if(strPressArg.length() > 0){
      strCurrentArg = strPressArg;
    }
    else{
      strCurrentArg = strLongPressArg;
      Sprint("Using long argument: ");
    }

    blinkCount = 1;

  }
  else{
    Sprint("It's LONG pin: ");
    Sprint(btnPin);
    Sprint(" , ");
    // if no argument for press, use longpress
    
    if(strLongPressArg.length() > 0){
      strCurrentArg = strLongPressArg;
    }
    else{
      strCurrentArg = strPressArg;
      Sprint("Using short argument: ");
    }

    blinkCount = 2;
  }

  Sprintln(strCurrentArg);

  // funzionante
  if(BtnData.Buttons[btnIndex].mqtt){
    MqttClient.publish("lights/lampadario/action/toggle","");
    return;
  }
  else{
    MakeRequestHttp(SettingsData.ifttt_key, strCurrentArg);
  }

  Blink(blinkCount, 200);

 }
