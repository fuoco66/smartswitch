

  // load the config from memory
DynamicJsonDocument WebGetFiles(){

	DynamicJsonDocument jsonDoc(1024);
	JsonObject root = jsonDoc.to<JsonObject>();
	
  FSInfo fs_info;
  SPIFFS.info(fs_info);
  
	root["totalBytes"] = fs_info.totalBytes;
	root["usedBytes"] = fs_info.usedBytes;

	JsonArray Arrfiles = root.createNestedArray("files");

  Dir dir = SPIFFS.openDir("/");
  while (dir.next()) {
   
		JsonObject file = Arrfiles.createNestedObject();
		file["path"] = dir.fileName();
		file["size"] = dir.fileSize();
		file["type"] = dir.isFile() ? "file" : "dir";

  }

	// serializeJson(jsonDoc, Serial);

	return jsonDoc;
}