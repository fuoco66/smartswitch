#include <FS.h>                   //this needs to be first, or it all crashes and burns...
#include <ESP8266WiFi.h>          //https://github.com/esp8266/Arduino
#include <DNSServer.h>
#include <ESP8266WebServer.h>
#include <ESP8266HTTPClient.h>
#include <WiFiClientSecure.h> 
#include <WiFiManager.h>          //https://github.com/tzapu/WiFiManager
#include <ArduinoJson.h>          //https://github.com/bblanchon/ArduinoJson
#include <ESP8266mDNS.h>
#include <ESP8266HTTPUpdateServer.h>
#include <Ticker.h>
#include <TimedAction.h>
#include <ezTime.h>
#include <PubSubClient.h>        //https://pubsubclient.knolleary.net/api

// #include "index.h" //Our HTML webpage contents with javascripts

#define STATUS_LED_1  15//2
#define STATUS_LED_2  2 //0 // made 0 to avoid built in led to light up. TO FIX, on off should be configurable

#define STATUS_LED_LEVEL_0  0//2
// #define STATUS_LED_LEVEL_1 512
#define STATUS_LED_LEVEL_1 128
#define STATUS_LED_LEVEL_2 1024

typedef struct HwBtn{
  uint8_t pin;
  bool active;
  bool mqtt;
  bool custom; // bustom behaviour
  char name[15];
  char pressArg[100];
  char longPressArg[100];
  uint8_t state; // btn State
  uint8_t secondState; // btn State
  uint8_t longPress; // btn State
  unsigned long lastDebounceTime;
  unsigned long longPressTime;
} HwBtn;

#define COUNT_HWBTN 10
#define DEBOUNCE_DELAY 500 // the debounce time; increase if the output flickers
#define LONG_PRESS_DELAY 700 // the debounce time; increase if the output flickers
#define LongP(x) (0-x)

// Define control switch pins
#define  BTN_1  5

// Example settings structure
struct Settings {

	char host[34];
	char update_username[40];
	char update_password[40];
	char update_path[34];
	char ifttt_key[34];
  // HwBtn btnData[];

} SettingsData = {
  // The default values
	"",
  "",
  "",
  "/firmware",
  ""
};

// Example settings structure
struct ButtonSettings {

  uint8_t maxBtnCount;
  uint8_t configBtnCount;
  HwBtn Buttons[COUNT_HWBTN];

} BtnData = {
  0,
  0,
  {}
};

struct ConfigurationSettings {
  int ledLevels[3];
  bool autoTimeDimm;
  bool secondaryStatusLed;
  uint8_t dayHour;
  uint8_t dayMinutes;
  uint8_t nightHour;
  uint8_t nightMinutes;
} ConfigurationData = {
  {0, 128, 1024},
  true,
  true,
  7,
  0,
  0, // night time
  0
};

typedef struct Account{
  uint8_t id;
  char email[100];
  char key[100];
} Account;

struct Accounts {

  uint8_t accountsCount;
  Account Accounts[10];

} AccountsData = {
  0,
  {}
};

void ReadSettings();
void SaveSettings();

void SetupBtn();
void CheckBtn();
int ReadBtn();
void MakeRequest();

void SetupConfigMode();
void configModeCallback();


#define DEBUG 1

#if DEBUG
  #define Sbegin(x) (Serial.begin(x))
  #define Sprintln(x) (Serial.println(x))
  #define Sprint(x) (Serial.print(x))
  #define Sprintf(x,y) (Serial.print(x))
#else
  #define Sbegin(x)
  #define Sprintln(x)
  #define Sprint(x)
#endif