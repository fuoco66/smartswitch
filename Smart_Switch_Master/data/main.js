
$(document).ready(function(){

  	//start manage loader
	$(document).ajaxSend(function(event, jqxhr, settings) {
		//default loader used
		$('#generalLoader').fadeIn(150);
	});

	$(document).ajaxComplete(function(event, jqxhr, settings) {
    $('#generalLoader').fadeOut(300);			
	});
  //end manage loader
  
  GetAllConfig();
  GetAllFiles();

});

var Paths = {
  osConfig: '/osConfig',
  osFile: '/osFile',
  fileUpload: '/fileUpload',
  osAction: 'osAction?OsAction=',
}

function pad(num, size) {
  var s = "000000000" + num;
  return s.substr(s.length-size);
}

function osAction(action) {
  var xhttp = new XMLHttpRequest();
  xhttp.open("GET", Paths.osAction + action, true);
  xhttp.send();
}

function GetAllConfig() {

  var dataJSON = {
    action: "getAllConfigs"
  }

  $.ajax({
    type: "POST",
    url: Paths.osConfig,
    data: JSON.stringify(dataJSON),
    dataType: "json",
    success: function(msg) {
      
      RenderConfig(msg.response.config);
      RenderBtns(msg.response.buttonsData);
      RenderAccounts(msg.response.accounts);

    },
    error: function(jqXHR,error, errorThrown) {  
      console.log("Something went wrong");
      
    }
  });
}

function GetBtnsConfig() {

  var dataJSON = {
    action: "getBtns"
  }

  $.ajax({
    type: "POST",
    url: Paths.osConfig,
    data: JSON.stringify(dataJSON),
    dataType: "json",
    success: function(msg) {
    
      RenderBtns(msg.response);
    
    },
    error: function(jqXHR,error, errorThrown) {  
      console.log("Something went wrong");
      
    }
  });
}

// edit creation
var arrSliders = [];

function RenderConfig(configData){
  // return;
  $('#mainConfig_autoTimeDimm').prop('checked', configData.autoTimeDimm );
  $('#mainConfig_secondaryStatusLed').prop('checked', configData.secondaryStatusLed );
  
  for (let i = 0; i < configData.ledLevels.length; i++) {
    const currentValue = configData.ledLevels[i];
    
    // creates new slider    
    if(arrSliders[i] == null){

      let slider = '';
      slider += '<div class="form-group">';
      slider += '  <label class="col-md-12 col-form-label" for="mainConfig_statusLed_' + i + '">Value ' + i + '</label>';
      slider += '  <div class="col-md-8">';
      slider += '    <div id="mainConfig_statusLed_' + i + '" data-valueId="' + i + '"></div>';
      slider += '  </div>';
      slider += '  <div class="col-md-4">';
      slider += '    <input id="mainConfig_statusLed_' + i + '_value" data-valueId="' + i + '" type="number" class="form-control"></input>';
      slider += '  </div>';
      slider += '</div>';

      $('#mainConfig_ledLevelsContainer').append(slider);

      // creates new slider
      arrSliders[i] = $('#mainConfig_statusLed_' + i).slider({
        orientation: 'horizontal',
        min: 0,
        max: 1024,
        slide: function( event, ui ) { 
          let senderIdValueId = $('#' + event.target.id).attr("data-valueId");
          $('#mainConfig_statusLed_' + senderIdValueId + '_value').val(ui.value);
        }
      });

      $('#mainConfig_statusLed_' + i + '_value' ).on( 'change', function() {

        let senderObj = $('#' + event.target.id);
        let senderIdValueId = senderObj.attr("data-valueId");
        
        arrSliders[senderIdValueId].slider( "option", "value", senderObj.val());    
      
      });

    }

    // set slider value
    arrSliders[i].slider( "option", "value", currentValue);    
    $('#mainConfig_statusLed_' + i + '_value').val(currentValue);

  }

  $('#mainConfig_dayTime').val(pad(configData.dayHour, 2) + ':' + pad(configData.dayMinutes, 2));
  $('#mainConfig_nightTime').val(pad(configData.nightHour, 2) + ':' + pad(configData.nightMinutes, 2));
  
}


function SaveConfigData(){

  var autoDimm = $('#mainConfig_autoTimeDimm').prop("checked");
  var secondaryStatusLed = $('#mainConfig_secondaryStatusLed').prop("checked");

  let dayTime = $('#mainConfig_dayTime').val();
  let dayArr = dayTime.split(':');
  let dayHour = parseInt(dayArr[0], 10);
  let dayMinutes = parseInt(dayArr[1], 10);

  let nightTime = $('#mainConfig_nightTime').val();
  let nightArr = nightTime.split(':');
  let nightHour = parseInt(nightArr[0], 10);
  let nightMinutes = parseInt(nightArr[1], 10);

  let arrLedLevels = [];
  for (let i = 0; i < arrSliders.length; i++) {
    arrLedLevels[i] = $('#mainConfig_statusLed_' + i + '_value').val();
  }

  let ledLevel_0 = $('#mainConfig_statusLed_0_value').val();    
  let ledLevel_1 = $('#mainConfig_statusLed_1_value').val();    
  let ledLevel_2 = $('#mainConfig_statusLed_2_value').val();    


  // get data
  var ConfigData = {
    ledLevels: arrLedLevels,
    ledLevel_0: parseInt(ledLevel_0, 10),
    ledLevel_1: parseInt(ledLevel_1, 10),
    ledLevel_2: parseInt(ledLevel_2, 10),
    autoTimeDimm: autoDimm,
    secondaryStatusLed: secondaryStatusLed,
    dayHour: dayHour,
    dayMinutes: dayMinutes,
    nightHour: nightHour,
    nightMinutes: nightMinutes
  };

  SaveConfigDataWebCall(ConfigData);
}

function SaveConfigDataWebCall(ConfigData){

  var dataJSON = {
    action: 'saveConfig',
    value: ConfigData
  }

  $.ajax({
    type: "POST",
    url: Paths.osConfig,
    data: JSON.stringify(dataJSON),
    dataType: "json",
    success: function(msg) {
      
      // reload interface
      RenderConfig(msg.response);
      
    },
    error: function(jqXHR,error, errorThrown) {  
      console.log("Something went wrong");
      
    }
  });

}

function RenderBtns(btnData){
  
  var btnDiv = "";

  var dummyBtn = {
    pin: '',
    active: false,
    mqtt: false,
    custom: false,
    name: '',
    pressArg: '',
    longPressArg: ''
  };

  
  $("#btnConfig_maxBtns").val(btnData.maxBtnCount);

  for (let i = 0; i < btnData.maxBtnCount; i++) {

    var currentBtn = dummyBtn;

    // if there are configured buttons, load the correct data
    // else it keeps the dummy one
    if (i < btnData.configBtnCount) {
      currentBtn = btnData['buttons'][i];
    }

    btnDiv += '<div class="col-md-3 col-sm-6 col-12 buttonDiv">';
    
    btnDiv += '<div class="innerDiv">';
      btnDiv += '<div class="form-group">';
        btnDiv += '<label for="btnId">Btn ' + i;
        btnDiv += '</label>';
      btnDiv += '</div>';

      var isActive = currentBtn.active ? 'checked' : '';
      btnDiv += '<div class="checkbox">';
        btnDiv += '<label>';
          btnDiv += '<input class="" id="btnData_active_' + i + '" type="checkbox" ' + isActive + ' /> Active';
        btnDiv += '</label>';
      btnDiv += '</div> ';
      
      var isMqtt = currentBtn.mqtt ? 'checked' : '';
      btnDiv += '<div class="checkbox">';
        btnDiv += '<label>';
          btnDiv += '<input class="" id="btnData_mqtt_' + i + '" type="checkbox" ' + isMqtt + ' /> Mqtt';
        btnDiv += '</label>';
      btnDiv += '</div> ';

      var isCustom = currentBtn.custom ? 'checked' : '';
      btnDiv += '<div class="checkbox">';
        btnDiv += '<label>';
          btnDiv += '<input class="" id="btnData_custom_' + i + '" type="checkbox" ' + isCustom + ' /> Custom';
        btnDiv += '</label>';
      btnDiv += '</div> ';

      btnDiv += '<div class="form-group">';
        btnDiv += '<label for="btnData_pin_' + i + '">Pin';
        btnDiv += '</label>';
        btnDiv += '<input type="text" class="form-control " id="btnData_pin_' + i + '" value="' + currentBtn.pin + '"/>';
      btnDiv += '</div>';

      btnDiv += '<div class="form-group">';
        btnDiv += '<label for="btnData_name_' + i + '">Name';
        btnDiv += '</label>';
        btnDiv += '<input type="text" class="form-control " id="btnData_name_' + i + '" value="' + currentBtn.name + '"/>';
      btnDiv += '</div>';

      btnDiv += '<div class="form-group">';
        btnDiv += '<label for="btnData_press_' + i + '">Press';
        btnDiv += '</label>';
        btnDiv += '<input type="text" class="form-control " id="btnData_press_' + i + '" value="' + currentBtn.pressArg + '"/>';
      btnDiv += '</div>';

      btnDiv += '<div class="form-group">';
        btnDiv += '<label for="btnData_longPress_' + i + '">Long Press';
        btnDiv += '</label>';
        btnDiv += '<input type="text" class="form-control " id="btnData_longPress_' + i + '" value="' + currentBtn.longPressArg + '"/>';
      btnDiv += '</div>';
      
    btnDiv += '</div>';
    btnDiv += '</div>';
          
    
  }

  $("#divButtonConfig_buttons").html(btnDiv);
  
}
  
function SaveBtnData(){

  let maxConfigBtns = parseInt($('#btnConfig_maxBtns').val());

  // get data
  var BtnsData = {
    maxBtnCount: maxConfigBtns,
    configBtnCount: 0,
    buttons :[]
  };


  for (let i = 0; i < maxConfigBtns; i++) {

    var active = $('#btnData_active_' + i).prop("checked");
    
    if(!active){
      continue;
    }

    var newBtn = {
      pin: parseInt($('#btnData_pin_' + i).val()),
      active: active,
      mqtt: $('#btnData_mqtt_' + i).prop("checked"),
      custom: $('#btnData_custom_' + i).prop("checked"),
      name: $('#btnData_name_' + i).val(),
      pressArg: $('#btnData_press_' + i).val(),
      longPressArg: $('#btnData_longPress_' + i).val()
    };

    BtnsData.configBtnCount = BtnsData.configBtnCount +1;
    BtnsData['buttons'][BtnsData.configBtnCount -1] = newBtn;

    
  }

  SaveBtnDataWebCall(BtnsData);

}

function SaveBtnDataWebCall(BtnData){

  var dataJSON = {
    action: 'saveBtns',
    value: BtnData
  }

  $.ajax({
    type: "POST",
    url: Paths.osConfig,
    data: JSON.stringify(dataJSON),
    dataType: "json",
    success: function(msg) {
      
      // reload interface
      RenderBtns(msg.response);
    
    },
    error: function(jqXHR,error, errorThrown) {  
      console.log("Something went wrong");
      
    }
  });

}

function GetAllFiles() {

  var dataJSON = {
    action: "getAllFiles"
  }

  $.ajax({
    type: "POST",
    url: Paths.osFile,
    data: JSON.stringify(dataJSON),
    dataType: "json",
    success: function(msg) {
      
      RenderFiles(msg.response);
          
    },
    error: function(jqXHR,error, errorThrown) {  
      console.log("Something went wrong");
      
    }
  });
}

function RenderFiles(filesData){

  var tableStr = '';

  $("#divUpdateFile_Total").html(filesData.totalBytes);
  $("#divUpdateFile_Used").html(filesData.usedBytes);

  for (let i = 0; i < filesData['files'].length; i++) {
    let currentFile = filesData['files'][i];
    tableStr += '<tr>';
    tableStr += ' <td>' + i + '</td>';
    tableStr += ' <td>' + currentFile.path + '</td>';
    tableStr += ' <td>' + currentFile.type + '</td>	';
    tableStr += ' <td>' + currentFile.size + '</td>	';
    tableStr += '</tr>';
  }

  $("#tableUpdateFile_filesTable > tbody").html(tableStr);
}

function RenderAccounts(accountsData){

  var newTable = "";
  var items = 0;

  for( i = 0; i < accountsData.accountsCount; i++ ) {

    var currentAccount = accountsData.accounts[i];

    newTable += '<tr data-local-id="' + i + '">\n';
    newTable += '	<td id="accountsTable_id_' + i + '" ><span>'+ currentAccount['id'] + '</span></td>\n';      
    newTable += '	<td id="accountsTable_email_' + i + '" ><span>'+ currentAccount['email'] + '</span></td>\n';      
    newTable += '	<td id="accountsTable_key_' + i + '" ><span>'+ currentAccount['key'] + '</span></td>\n';
    newTable += '</tr>';
  }

  $("#mainConfig_accountsTable tbody").empty();
  $("#mainConfig_accountsTable tbody").html( newTable );

  InitializeTableEdit('mainConfig_accountsTable');

  var addAccount = '<tr data-local-id="' + (i+1) + '">\n';
  addAccount += '	<td><span>New</span></td>\n';
  addAccount += '	<td><input id="accountsTable_add_email" data-attribute="email" class="tabledit-input form-control input-sm"></input></td>\n';
  addAccount += '	<td><input id="accountsTable_add_key" data-attribute="key" class="tabledit-input form-control input-sm"></input></td>\n';
  addAccount += '	<td><button id="btnAddAccount" class="btn btn-block btn-success">Add</button></td>\n';
  addAccount += '</tr>';

  $("#mainConfig_accountsTable tbody").append( addAccount );

  $('#btnAddAccount').unbind();
  $('#btnAddAccount').click(function(e){
    AddAccounts();
  });

}

function InitializeTableEdit(table){
$('#' + table).Tabledit({
  url: Paths.osConfig,  // those are dummy, the ajax web call is not performed here
  ajaxJsonFormat: true,
  restoreButton: false,
  mutedClass: 'success-fadeOut',
  columns: {
      identifier: [0, 'id'],
      editable: [[1, 'email'], [2, 'key']]
  },
  buttons: {
      edit: {
        action: 'saveAccounts'
      },
      delete: {
        action: 'deleteAccounts'
      },
      save: {
        class: 'btn btn-sm btn-success',
        html: 'Save'
    },
  },
  onAjax:  function(action, serialize) {
    // here the a
    //return false;
    var deleteIndex = null;

    if(action == 'deleteAccounts'){
      deleteIndex = parseInt(serialize.strPayload.substring(3, serialize.strPayload.indexOf('&')));
    }

    var newPayLoad = {
      action: 'saveAccounts',
      value: GetAccountData(false, deleteIndex)
    };

    serialize.strPayload = JSON.stringify(newPayLoad);
    
  },
  onSuccess: function(data, textStatus, jqXHR, action) {
    
    RenderAccounts(data.response);

  },
  onFail: function(jqXHR, textStatus, errorThrown) {
      console.log('onFail(jqXHR, textStatus, errorThrown)');
      console.log(jqXHR);
      console.log(textStatus);
      console.log(errorThrown);
  }

});
}

function GetAccountData(addAccount = false, deleteAccountId = null){

  // get elements count, minus one because the "add" and thead
  var accountsCount = $("#mainConfig_accountsTable tr").length -2;

  // return object
  var accountsData = {
    accountsCount: accountsCount,
    accounts: []
  }

  var lastId = -1;

  for (let i = 0; i < accountsCount; i++) {
    let currentId = parseInt($('#accountsTable_id_' + i + ' input').val());

    if(deleteAccountId != null && currentId == deleteAccountId){
      continue;
    }

    var newAccount = {
      id: currentId,
      email: $('#accountsTable_email_' + i + ' input').val(),
      key: $('#accountsTable_key_' + i + ' input').val()
    };

    accountsData['accounts'][i] = newAccount;
    lastId = i;
  }

  if(addAccount){

    let newId = lastId + 1;

    var newAccount = {
      id: newId,
      email: $('#accountsTable_add_email').val(),
      key: $('#accountsTable_add_key').val()
    };

    accountsData['accounts'][newId] = newAccount;
  }

  accountsData.accountsCount = accountsData.accounts.length;

  return accountsData;

}

function AddAccounts(){
  
  var dataJSON = {
    action: 'saveAccounts',
    value: GetAccountData(true, null)
  }

  $.ajax({
    type: "POST",
    url: Paths.osConfig,
    data: JSON.stringify(dataJSON),
    dataType: "json",
    success: function(msg) {
      
      // reload interface
      RenderAccounts(msg.response);
    
    },
    error: function(jqXHR,error, errorThrown) {  
      console.log("Something went wrong");
      
    }
  });


}
