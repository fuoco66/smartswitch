
#define SECURE 0

#if !SECURE

  String MakeRequestHttp(String _key, String _argument){
    
    if(_key.length() == 0 ||
       _argument.length() == 0){
      return "FAIL_NO_ARGS";
    }

    // HTTP
    HTTPClient http;  //Declare an object of class HTTPClient
    
    http.begin("http://maker.ifttt.com/trigger/" + _argument + "/with/key/" + _key);  //Specify request destination
    int httpCode = http.GET();                                                                  //Send the request

    String returnMsg;  

    //Check the returning code
    if (httpCode > 0) { 
    
      returnMsg = http.getString();   //Get the request response payload
      // Sprintln(payload);                     //Print the response payload
    
    } else{
      returnMsg = String(httpCode);    
    }
    
    http.end();   //Close connection
    return returnMsg;
  }

#else 
  // const char *host = "maker.ifttt.com";
  // const int httpsPort = 443;  //HTTPS= 443 and HTTP = 80

  // //SHA1 finger print of certificate use web browser to view and copy
  // //ifttt
  // const char fingerprint[] PROGMEM = "AA 75 CB 41 2E D5 F9 97 FF 5D A0 8B 7D AC 12 21 08 4B 00 8C";

  // void MakeRequestHttp(){
    
  //   WiFiClientSecure httpsClient;    //Declare object of class WiFiClient

  //   Sprintln(host);

  //   Serial.printf("Using fingerprint '%s'\n", fingerprint);
  //   httpsClient.setFingerprint(fingerprint);
  //   httpsClient.setTimeout(15000); // 15 Seconds
  //   delay(1000);
    
  //   Serial.print("HTTPS Connecting");
  //   int r=0; //retry counter
  //   while((!httpsClient.connect(host, httpsPort)) && (r < 30)){
  //     delay(100);
  //     Serial.print(".");
  //     r++;
  //   }
  //   if(r==30) {
  //     Sprintln("Connection failed");
  //   }
  //   else {
  //     Sprintln("Connected to web");
  //   }
    
  //   String ADCData, getData, Link;
  //   int adcvalue=7;//analogRead(A0);  //Read Analog value of LDR
  //   ADCData = String(adcvalue);   //String to interger conversion

  //   //GET Data
  //   Link = "/comments?postId=" + ADCData;
  //   Link = "/trigger/lampadario_on/with/key/kGt7Gxo9RU2DnTCzvw9ae";


  //   Serial.print("requesting URL: ");
  //   Sprintln(host+Link);

  //   httpsClient.print(String("GET ") + Link + " HTTP/1.1\r\n" +
  //               "Host: " + host + "\r\n" +               
  //               "Connection: close\r\n\r\n");

  //   Sprintln("request sent");
                    
  //   while (httpsClient.connected()) {
  //     String line = httpsClient.readStringUntil('\n');
  //     if (line == "\r") {
  //       Sprintln("headers received");
  //       break;
  //     }
  //   }

  //   Sprintln("reply was:");
  //   Sprintln("==========");
  //   String line;
  //   // while(httpsClient.available()){        
  //   //   line = httpsClient.readStringUntil('\n');  //Read Line by Line
  //   //   Sprintln(line); //Print response
  //   // }
  //   Sprintln("==========");
  //   Sprintln("closing connection");
      
  //   // delay(2000);  //GET Data at every 2 seconds

  }

#endif

//gets called when WiFiManager enters configuration mode
void configModeCallback (WiFiManager *myWiFiManager) {
  StatusLedTicker.attach(1, tick);
}

//callback notifying us of the need to save config
void saveConfigCallback () {
  shouldSaveConfig = true;
}

void SetupConfigMode(WiFiManager& wifiManager){
  //set callback that gets called when connecting to previous WiFi fails, and enters Access Point mode
  wifiManager.setAPCallback(configModeCallback);

  //set config save notify callback
  wifiManager.setSaveConfigCallback(saveConfigCallback);

  // The extra parameters to be configured (can be either global or just in the setup)
  // After connecting, parameter.getValue() will get you the configured value
  // id/name placeholder/prompt default length
  WiFiManagerParameter custom_text0("<p>Select your wifi network and type in your password, if you do not see your wifi then scroll down to the bottom and press scan to check again.</p>");
  WiFiManagerParameter custom_host_title("<h1>Hostname</h1>");
  WiFiManagerParameter custom_host_p1("<p>Enter a name for this device which will be used for the hostname on your network and identify the device from MQTT.</p>");
  WiFiManagerParameter custom_host_value("name", "Device Name", SettingsData.host, 32);

  WiFiManagerParameter custom_ifttt_title("<h1>IFTTT ID</h1>");
  WiFiManagerParameter custom_ifttt_p1("<p>Enter the ifttt key.</p>");
  WiFiManagerParameter custom_ifttt_value("ifttt_key", "Ifttt key", SettingsData.ifttt_key, 32);
  
  WiFiManagerParameter custom_text5("<h1>Web Updater</h1>");
  WiFiManagerParameter custom_text6("<p>The web updater allows you to update the firmware of the device via a web browser by going to its ip address or hostname /firmware ex. 192.168.0.5/firmware you can change the update path below. The update page is protected so enter a username and password you would like to use to access it. </p>");
  WiFiManagerParameter custom_update_username("user", "Username For Web Updater", SettingsData.update_username, 40);
  WiFiManagerParameter custom_update_password("password", "Password For Web Updater", SettingsData.update_password, 40);
  WiFiManagerParameter custom_device_path("path", "Updater Path", SettingsData.update_path, 32);
  WiFiManagerParameter custom_text7("<p>*To reset device settings restart the device and quickly move the jumper from RUN to PGM, wait 10 seconds and put the jumper back to RUN.*</p>");
  WiFiManagerParameter custom_text8("");


  //set static ip
  //_manager.setSTAStaticIPConfig(IPAddress(10,0,1,99), IPAddress(10,0,1,1), IPAddress(255,255,255,0));
  
  //add all your parameters here
  wifiManager.setCustomHeadElement("<style>.c{text-align: center;} div,input{padding:5px;font-size:1em;} input{width:95%;} body{text-align: center;font-family:oswald;} button{border:0;background-color:#313131;color:white;line-height:2.4rem;font-size:1.2rem;text-transform: uppercase;width:100%;font-family:oswald;} .q{float: right;width: 65px;text-align: right;} body{background-color: #575757;}h1 {color: white; font-family: oswald;}p {color: white; font-family: open+sans;}a {color: #78C5EF; text-align: center;line-height:2.4rem;font-size:1.2rem;font-family:oswald;}</style>");
  wifiManager.addParameter(&custom_text0);
  wifiManager.addParameter(&custom_host_title);
  wifiManager.addParameter(&custom_host_p1);
  wifiManager.addParameter(&custom_host_value);
  wifiManager.addParameter(&custom_ifttt_title);
  wifiManager.addParameter(&custom_ifttt_p1);
  wifiManager.addParameter(&custom_ifttt_value);

  wifiManager.addParameter(&custom_text5);
  wifiManager.addParameter(&custom_text6);
  wifiManager.addParameter(&custom_update_username);
  wifiManager.addParameter(&custom_update_password);
  wifiManager.addParameter(&custom_device_path);
  wifiManager.addParameter(&custom_text7);
  wifiManager.addParameter(&custom_text8);
  
  wifiManager.setTimeout(120);
  
  //fetches ssid and pass and tries to connect
  //if it does not connect it starts an access point with the specified name
  //here  "AutoConnectAP"
  //and goes into a blocking loop awaiting configuration
  if (!wifiManager.autoConnect(ssidAP.c_str())) {
    delay(3000);
    //reset and try again, or maybe put it to deep sleep
    ESP.reset();
    delay(5000);
  }

  //if you get here you have connected to the WiFi
  Sprintln("connected...yeey :)");
  
  
  if (shouldSaveConfig) {
    strcpy(SettingsData.host, custom_host_value.getValue());
    strcpy(SettingsData.ifttt_key, custom_ifttt_value.getValue());
    strcpy(SettingsData.update_username, custom_update_username.getValue());
    strcpy(SettingsData.update_password, custom_update_password.getValue());
    strcpy(SettingsData.update_path, custom_device_path.getValue());
  
    SaveSettings();
  }

  delay(5000);

  // verify if params are complete
  if(digitalRead(0) == LOW ||
    String(SettingsData.host).length() == 0 ||
    String(SettingsData.ifttt_key).length() == 0 ||
    String(SettingsData.update_username).length() == 0 ||
    String(SettingsData.update_password).length() == 0){

    Sprint("host: ");
    Sprintln(SettingsData.host);
    
    Sprint("usr: ");
    Sprintln(SettingsData.update_username);
    
    Sprint("psw: ");
    Sprintln(SettingsData.update_password);
    
    Sprint("dig: ");
    Sprintln(digitalRead(0));
        
    Sprintln("Reset, Params corrupted");
    
    FullReset();
    // wifiManager.resetSettings();
    // ESP.reset();
  }

  StatusLedTicker.detach();
  //keep LED on
  FixedLed();


}

void HandleWebRoot(){

  ObjGlobal_HttpServer.sendHeader("Location", "/index.html",true);   //Redirect to our html web page
  ObjGlobal_HttpServer.send(302, "text/plane","");
}

void handleWebRequests(){  
  if(loadFromSpiffs(ObjGlobal_HttpServer.uri())) return;
  String message = "File Not Detected\n\n";
  message += "URI: ";
  message += ObjGlobal_HttpServer.uri();
  message += "\nMethod: ";
  message += (ObjGlobal_HttpServer.method() == HTTP_GET)?"GET":"POST";
  message += "\nArguments: ";
  message += ObjGlobal_HttpServer.args();
  message += "\n";
  for (uint8_t i=0; i<ObjGlobal_HttpServer.args(); i++){
    message += " NAME:"+ObjGlobal_HttpServer.argName(i) + "\n VALUE:" + ObjGlobal_HttpServer.arg(i) + "\n";
  }
  ObjGlobal_HttpServer.send(404, "text/plain", message);
  Serial.println(message);
}

bool loadFromSpiffs(String path){
  String dataType = "text/plain";
  if(path.endsWith("/")) path += "index.htm";

  if(path.endsWith(".src")) path = path.substring(0, path.lastIndexOf("."));
  else if(path.endsWith(".html")) dataType = "text/html";
  else if(path.endsWith(".htm")) dataType = "text/html";
  else if(path.endsWith(".css")) dataType = "text/css";
  else if(path.endsWith(".js")) dataType = "application/javascript";
  else if(path.endsWith(".png")) dataType = "image/png";
  else if(path.endsWith(".gif")) dataType = "image/gif";
  else if(path.endsWith(".jpg")) dataType = "image/jpeg";
  else if(path.endsWith(".ico")) dataType = "image/x-icon";
  else if(path.endsWith(".xml")) dataType = "text/xml";
  else if(path.endsWith(".pdf")) dataType = "application/pdf";
  else if(path.endsWith(".zip")) dataType = "application/zip";
  File dataFile = SPIFFS.open(path.c_str(), "r");
  if (ObjGlobal_HttpServer.hasArg("download")) dataType = "application/octet-stream";
  if (ObjGlobal_HttpServer.streamFile(dataFile, dataType) != dataFile.size()) {
  }

  dataFile.close();
  return true;
}

void HandleOsAction(){

  String strAction = ObjGlobal_HttpServer.arg("OsAction");
    Sprintln(strAction);

  if(strAction == "0"){
    Sprintln("Wifi reset");
    WifiReset();
  }
  else if(strAction == "1"){
    Sprintln("Full Reset");
    FullReset();
  }
  else if(strAction == "2"){
    Sprintln("Reboot");
    Reboot();
  }  
}

void HandleOsConfig(){

  // get payload
  String strPayload = ObjGlobal_HttpServer.arg("plain");

  // check payload
  if(strPayload == "" || strPayload.length() == 0 || strPayload == "\"\"" || strPayload == "null"){
    Sprintln(strPayload);
    ObjGlobal_HttpServer.send(200, "text/json","{\"code\": \"-1\", \"response\":\"EMPTY_PAYLOAD\"}");
    return;
  }

  // parse argument
  DynamicJsonDocument objPayload(1024);
  DeserializationError jsonError = deserializeJson(objPayload, strPayload);
			
  if (jsonError){
    Sprintln("failed to load json config");
    ObjGlobal_HttpServer.send(200, "text/json","{\"code\": \"-4\", \"response\":\"DECODE_FAILED\"}");
    return;
  }
  // release some memory
  objPayload.shrinkToFit();

  // check action
  if(objPayload["action"] == "" || objPayload["action"] == "\"\"" || objPayload["action"] == "null"){
    Sprintln(strPayload);
    ObjGlobal_HttpServer.send(200, "text/json","{\"code\": \"-2\", \"response\":\"EMPTY_ACTION\"}");
    return;
  }

  if(objPayload["action"] == "getAllConfigs"){

    String strConfig;
	  serializeJson(WebGetConfig(), strConfig);

    String strButtonsConfig;
	  serializeJson(WebGetButtonsConfig(), strButtonsConfig);

    String strAccounts;
	  serializeJson(WebGetAccounts(), strAccounts);

    String webResponse = "{ \"config\":" + strConfig + ",  \"accounts\":" + strAccounts + ", \"buttonsData\":" + strButtonsConfig + " }";

	  ObjGlobal_HttpServer.send(200, "text/json","{\"code\": \"0\", \"response\":" + webResponse + "}");

  }
  else if(objPayload["action"] == "getBtns"){
    // serializeJson(WebGetButtonsConfig(), Serial);

    String webResponse;
	  serializeJson(WebGetButtonsConfig(), webResponse);
	  ObjGlobal_HttpServer.send(200, "text/json","{\"code\": \"0\", \"response\":" + webResponse + "}");

  }
  else if(objPayload["action"] == "getConfig"){
    // serializeJson(WebGetButtonsConfig(), Serial);

    String webResponse;
	  serializeJson(WebGetConfig(), webResponse);
	  ObjGlobal_HttpServer.send(200, "text/json","{\"code\": \"0\", \"response\":" + webResponse + "}");

  }
  else if(objPayload["action"] == "saveConfig"){

    SaveConfigFile(strPayload, configfile);
    // serializeJson(WebGetButtonsConfig(), Serial);
	  //reload configuration in memory
	  LoadConfig();

    // reload timed events
    SetupTime();
    
    String webResponse;
    serializeJson(WebGetConfig(), webResponse);
    ObjGlobal_HttpServer.send(200, "text/json","{\"code\": \"0\", \"response\":" + webResponse + "}");

  } 
  else if(objPayload["action"] == "saveBtns"){

    SaveConfigFile(strPayload, buttonsFile);
    // serializeJson(WebGetButtonsConfig(), Serial);
  
	  //reload configuration in memory
	  LoadButtonsConfig();

    String webResponse;
    serializeJson(WebGetButtonsConfig(), webResponse);
    ObjGlobal_HttpServer.send(200, "text/json","{\"code\": \"0\", \"response\":" + webResponse + "}");

  }
  else if(objPayload["action"] == "getAccounts"){
    // serializeJson(WebGetButtonsConfig(), Serial);

    String webResponse;
	  serializeJson(WebGetAccounts(), webResponse);
	  ObjGlobal_HttpServer.send(200, "text/json","{\"code\": \"0\", \"response\":" + webResponse + "}");

  }
  else if(objPayload["action"] == "saveAccounts"){

    SaveConfigFile(strPayload, accountsFile);
    // serializeJson(WebGetButtonsConfig(), Serial);
	  //reload configuration in memory
	  LoadAccounts();

    String webResponse;
    serializeJson(WebGetAccounts(), webResponse);
    ObjGlobal_HttpServer.send(200, "text/json","{\"code\": \"0\", \"response\":" + webResponse + "}");

  } 
  else{
    Sprintln(strPayload);

    ObjGlobal_HttpServer.send(200, "text/json","{\"code\": \"-3\", \"response\":\"ACTION_NOT_FOUND\"}");
  }

}

void HandleOsFile(){

  // get payload
  String strPayload = ObjGlobal_HttpServer.arg("plain");

  // check payload
  if(strPayload == "" || strPayload.length() == 0 || strPayload == "\"\"" || strPayload == "null"){
    Sprintln(strPayload);
    ObjGlobal_HttpServer.send(200, "text/json","{\"code\": \"-5\", \"response\":\"EMPTY_PAYLOAD\"}");
    return;
  }

  // parse argument
  DynamicJsonDocument objPayload(1024);
  DeserializationError jsonError = deserializeJson(objPayload, strPayload);
			
  if (jsonError){
    Sprintln("failed to load json config");
    ObjGlobal_HttpServer.send(200, "text/json","{\"code\": \"-6\", \"response\":\"DECODE_FAILED\"}");
    return;
  }
  // release some memory
  objPayload.shrinkToFit();

  // check action
  if(objPayload["action"] == "" || objPayload["action"] == "\"\"" || objPayload["action"] == "null"){
    Sprintln(strPayload);
    ObjGlobal_HttpServer.send(200, "text/json","{\"code\": \"-7\", \"response\":\"EMPTY_ACTION\"}");
    return;
  }

  if(objPayload["action"] == "getAllFiles"){

    String strFiles;
	  serializeJson(WebGetFiles(), strFiles);

    String webResponse = strFiles;

	  // ObjGlobal_HttpServer.send(200, "text/json","{\"code\": \"0\", \"response\":'banana'}");
	  ObjGlobal_HttpServer.send(200, "text/json","{\"code\": \"0\", \"response\":" + webResponse + "}");

  }
  else{
    Sprintln(strPayload);

    ObjGlobal_HttpServer.send(200, "text/json","{\"code\": \"-8\", \"response\":\"ACTION_NOT_FOUND\"}");
  }

}

void SetupWebHandlers(){
  // setup root page
  
  ObjGlobal_HttpServer.on("/", [](){
    if(!ObjGlobal_HttpServer.authenticate(SettingsData.update_username, SettingsData.update_password))
      return ObjGlobal_HttpServer.requestAuthentication();
    HandleWebRoot();
  });

  ObjGlobal_HttpServer.on("/osAction", [](){
    if(!ObjGlobal_HttpServer.authenticate(SettingsData.update_username, SettingsData.update_password))
      return ObjGlobal_HttpServer.requestAuthentication();
    
    HandleOsAction();

  });

  ObjGlobal_HttpServer.on("/osFile", [](){
      if(!ObjGlobal_HttpServer.authenticate(SettingsData.update_username, SettingsData.update_password))
        return ObjGlobal_HttpServer.requestAuthentication();
      
      HandleOsFile();

  });

  // setup reset page
  ObjGlobal_HttpServer.on("/osConfig", [](){
    if(!ObjGlobal_HttpServer.authenticate(SettingsData.update_username, SettingsData.update_password))
      return ObjGlobal_HttpServer.requestAuthentication();
    
    HandleOsConfig();

  });

  ObjGlobal_HttpServer.onNotFound(handleWebRequests);

  ObjGlobal_HttpServer.begin();
  
}