
// #include "Hardware.h"

void SetupBtn(){
  
  for (uint8_t i = 0; i < BtnData.configBtnCount; i++){
    // InputBtn[i] = (HwBtn){BtnData.Buttons[i].pin, false, LOW, LOW, LOW, 0, 0};
    
    pinMode(BtnData.Buttons[i].pin, INPUT);
  }

}

void CheckBtn(){
  //Sprintln("run");
  // look for pressed button
  for (uint8_t i = 0; i < BtnData.configBtnCount; i++){

    if(digitalRead(BtnData.Buttons[i].pin) == LOW){
      BtnData.Buttons[i].state = LOW;
      continue;
    }

    // check for debounce
    if ((millis() - BtnData.Buttons[i].lastDebounceTime) > DEBOUNCE_DELAY) {
    
      // update pressed time
      BtnData.Buttons[i].lastDebounceTime = millis();

      // pressed button found
      BtnData.Buttons[i].state = HIGH;
      continue;
    }    


  }
}

int ReadBtn(){

  int returnButton = 99;

  // look for pressed button
  for (uint8_t i = 0; i < BtnData.configBtnCount; i++){
    //Sprintln(InputBtn[i].state);
    
    // pressed button found
    if(BtnData.Buttons[i].state == HIGH){

      // it's the first time the button is read as pressed
      if(BtnData.Buttons[i].secondState == LOW){
        // change secondState to mark that's already been read as pressed
        BtnData.Buttons[i].secondState = HIGH;
        // start timer for long press
        BtnData.Buttons[i].longPressTime = millis();
        continue;
      }

      // N-time the button is read as pressed, check time
      // If time limit is reached check if it's the first time we encounter the long press
      if ((millis() - BtnData.Buttons[i].longPressTime) > LONG_PRESS_DELAY &&
           BtnData.Buttons[i].longPress == LOW) {
        
        // reset the  
        // BtnData.Buttons[i].secondState = LOW;
        // mark that we registered the long press
        BtnData.Buttons[i].longPress = HIGH;
        
        // send the pin number converted to negative
        returnButton = LongP(BtnData.Buttons[i].pin);
        break;
      }

    } // the button has been released, and already pressed once 
    else if(BtnData.Buttons[i].state == LOW && BtnData.Buttons[i].secondState == HIGH){

      // check if we already registered a long press
      if(BtnData.Buttons[i].longPress == LOW){
        //  returns the pin number 
        returnButton = BtnData.Buttons[i].pin;
      }

      // resets variables
      BtnData.Buttons[i].secondState = LOW;
      BtnData.Buttons[i].longPress = LOW;
      BtnData.Buttons[i].longPressTime = 0;

      break;
    }
    
  }
  return returnButton;
}
