const char MAIN_page[] = R"=====(
<!DOCTYPE html>
<html>
<head>
  <link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css'>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js'></script>"
  <!-- <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous"> -->
  <script>

    function RenderBtns(){
    
      var btnDiv = "";
    
      for (var i = 0; i < 3; i++) {
  
        btnDiv += '<div class="col-md-4">';
          btnDiv += '<div class="form-group">';
            btnDiv += '<label for="btnId">Btn Id';
            btnDiv += '</label>';
          btnDiv += '</div>';
  
          btnDiv += '<div class="checkbox">';
            btnDiv += '<label>';
              btnDiv += '<input type="checkbox" /> Active';
            btnDiv += '</label>';
          btnDiv += '</div> ';
          
          btnDiv += '<div class="form-group">';
            btnDiv += '<label for="pin">pin';
            btnDiv += '</label>';
            btnDiv += '<input type="text" class="form-control" id="pin" />';
          btnDiv += '</div>';
  
          btnDiv += '<div class="form-group">';
            btnDiv += '<label for="name">name';
            btnDiv += '</label>';
            btnDiv += '<input type="text" class="form-control" id="name" />';
          btnDiv += '</div>';
  
          btnDiv += '<div class="form-group">';
            btnDiv += '<label for="singlePress">singlePress';
            btnDiv += '</label>';
            btnDiv += '<input type="text" class="form-control" id="singlePress" />';
          btnDiv += '</div>';
  
          btnDiv += '<div class="form-group">';
            btnDiv += '<label for="longPress">longPress';
            btnDiv += '</label>';
            btnDiv += '<input type="text" class="form-control" id="longPress" />';
          btnDiv += '</div>';
          
        btnDiv += '</div>';
              
        $("#divButtons").html(btnDiv);
      
      }
  
      
  
  
  
    }
  
    function osAction(action) {
      var xhttp = new XMLHttpRequest();
      xhttp.open("GET", "osAction?OsAction=" + action, true);
      xhttp.send();
    }
  
    
    // function getData() {
  
    //   var xhttp = new XMLHttpRequest();
    //   xhttp.onreadystatechange = function () {
    //     if (this.readyState == 4 && this.status == 200) {
    //       document.getElementById("WaterState").innerHTML = this.responseText == 1 ? "LOW" : "GOOD";
    //     }
    //   };
    //   xhttp.open("GET", "readSensor", true);
    //   xhttp.send();
  
    //   var xhttp = new XMLHttpRequest();
    //   xhttp.onreadystatechange = function () {
    //     if (this.readyState == 4 && this.status == 200) {
    //       document.getElementById("PumpState").innerHTML =
    //         this.responseText;
    //     }
    //   };
    //   xhttp.open("GET", "readPump", true);
    //   xhttp.send();
    // }
    
    // function countdown(element, minutes, seconds) {
    //   // Fetch the display element
    //   var el = document.getElementById(element);
  
    //   // Set the timer
    //   var interval = setInterval(function () {
    //     if (seconds == 0) {
    //       if (minutes == 0) {
            
    //         //stop the pump
    //         sendData(0);
            
    //         (el.innerHTML = "STOP!");
    //         clearInterval(interval);
    //         return;
    //       } else {
    //         minutes--;
    //         seconds = 60;
    //       }
    //     }
  
    //     if (minutes > 0) {
    //       var minute_text = minutes + (minutes > 1 ? ' minutes' : ' minute');
    //     } else {
    //       var minute_text = '';
    //     }
  
    //     var second_text = seconds > 1 ? '' : '';
    //     el.innerHTML = minute_text + ' ' + seconds + ' ' + second_text + '';
    //     seconds--;
    //   }, 1000);
    // }
  
  
    setInterval(function () {
      // Call a function repetatively with 2 Second interval
      //getData();
    }, 1000); //2000mSeconds update rate
  
  
  </script>
  

</head>


<body>

  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <h3 class="text-center">
          Smart Switch
        </h3>
      </div>
    </div>
  
    <div class="row" id="divButtons">
    <script>RenderBtns();</script>
        
      <!-- <div class="col-md-4">
          <div class="form-group">
            <label for="btnId">
              Btn Id
            </label>
          </div>

          <div class="checkbox">
            <label>
              <input type="checkbox" /> Active
            </label>
          </div> 
          
          <div class="form-group">
            <label for="pin">
              pin
            </label>
            <input type="text" class="form-control" id="pin" />
          </div>

          <div class="form-group">
            <label for="name">
              name
            </label>
            <input type="text" class="form-control" id="name" />
          </div>

          <div class="form-group">
            <label for="singlePress">
              singlePress
            </label>
            <input type="text" class="form-control" id="singlePress" />
          </div>

          <div class="form-group">
            <label for="longPress">
              longPress
            </label>
            <input type="text" class="form-control" id="longPress" />
          </div>
          
      </div> -->
        
    </div>

    <div class="row">
      <div class="col-md-12">
          
        <button type="button" class="btn btn-success btn-block">
          Button
        </button>
      </div>
    </div>
    
    <div class="row">
      <div class="col-md-4">
         
        <button type="button" onclick="osAction(2)" class="btn btn-md btn-primary btn-block">
          Reboot
        </button>
      </div>
      <div class="col-md-4">
         
        <button type="button" onclick="osAction(0)" class="btn btn-block btn-primary">
          Reset Wifi
        </button>
      </div>
      <div class="col-md-4">
         
        <button type="button" onclick="osAction(1)" class="btn btn-danger btn-block">
          Full Reset
        </button>
      </div>
    </div>
    
  </div>

  <!-- <div class="container-fluid">
    <div class="col-sm-12">
      <span>Water Level is :</span>
      <span id="WaterState">0</span>
    </div>
    <div class="col-sm-12">
      <span>PumpState is :</span>
      <span id="PumpState">NA</span>
    </div>
  </div> -->
  
  
</body>

</html>
)=====";
